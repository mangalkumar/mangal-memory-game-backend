const router = require('express').Router()
const path = require('path')

const executeQuery = require(path.join(__dirname,'../executeQuery'))

router.get('/',async (req, res)=>{
    try{
        const query = "select * from users;"

        const result = await executeQuery(query)

        res.status(200).json(result)

    }catch(err){
        res.json({
            message: "While processing your request, technical issue occurred. Please try again later."
        })
    }
})

router.post('/',async (req, res)=>{
    try{
        const query = `insert into users(name, level, score) values ('${req.body.name}', ${parseInt(req.body.level)}, ${parseInt(req.body.score)});`

        const result = await executeQuery(query)

        res.status(200).json({
            status: 200,
            message: "Data Inserted to database"
        })

    }catch(err){
        console.log(err.message)
    }
})

module.exports = router