const path = require('path')

const pool =  require(path.join(__dirname ,'./connection'))

function executeQuery(query){
    return new Promise((resolve, reject)=>{
        // console.log("inside promise", pool)
        pool.getConnection(function(err, connection) {
            if (err){
                return reject(err) // not connected!
            } 
        
            // Use the connection
            return connection.query(query, function (error, results, fields) {
                // When done with the connection, release it.
                if (error) {
                    return reject( new Error(error.message)) // not connected!
                }
                resolve(results)

                connection.release();
        
                // Handle error after the release.
               
                
            });
        });
    })
}

module.exports = executeQuery