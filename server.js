const express = require('express')
const env = require('./config')
const api = require('./routes/api')
const bodyParser = require('body-parser')
const path = require('path')
const cors = require('cors')
const app = express()
const executeQuery = require('./executeQuery')

//Making tables if not exist

const query = "CREATE TABLE IF NOT EXISTS users (id int primary key auto_increment, name varchar(100), level int, score int);"
executeQuery(query)
.then(()=>{
    console.log('Table created if not exist')
})
.catch((err)=>{
    console.log(err)
})

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.get('/',(req, res)=>{
    res.sendFile(path.join(__dirname, './index.html'))
})

app.use('/api',api)

app.get('*',(req, res)=>{
    res.status(404).json({
        message: "Route Not Found  , Try another Route"
    })
})

app.listen(env.PORT)