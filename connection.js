const mysql = require('mysql')

const env = require('./config')

const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : env.DB_HOST,
    user            : env.DB_USER,
    password        : env.DB_PASS,
    database        : env.DB_NAME
  });

  module.exports = pool